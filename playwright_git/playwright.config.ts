import { PlaywrightTestConfig, devices } from '@playwright/test';

const config: PlaywrightTestConfig = {
  forbidOnly: !!process.env.CI,
  retries: process.env.CI ? 2 : 0,
  use: {
    trace: 'retain-on-failure',
    video: 'on',
    headless: true,
   // viewport: { width: 1280, height: 720 },
    ignoreHTTPSErrors: false,
    actionTimeout : 1000 * 30,
    baseURL : 'https://playwright.dev/',
    offline : false,
    screenshot : 'on',
    // proxy : {
    //   server : '',
    // }
    
  },
  projects: [

// "Pixel 4" tests use Chromium browser.
    {
      name: 'Pixel 4',
      use: {
        browserName: 'chromium',
        ...devices['Pixel 4'],
      },
    },

    {
      name: 'chromium',
      use: { ...devices['Desktop Chrome'] },
    },
    {
      name: 'firefox',
      use: { ...devices['Desktop Firefox'] },
    },
    {
      name: 'webkit',
      use: { ...devices['Desktop Safari'] },
    },
  ],
};
export default config;